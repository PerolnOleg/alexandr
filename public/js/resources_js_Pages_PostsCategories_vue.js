(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_PostsCategories_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PostsCategories.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PostsCategories.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "PostsCategories",
  props: ['slug'],
  data: function data() {
    return {
      category: null,
      loading: false,
      posts: null,
      error: null,
      perPage: 2,
      currentPage: 1,
      rows: 0
    };
  },
  created: function created() {
    this.fetchData();
  },
  watch: {
    $route: 'fetchData'
  },
  methods: {
    fetchData: function fetchData() {
      var _this = this;

      this.$axios.get('/api/categories/' + this.slug).then(function (response) {
        _this.category = response.data.data;
        _this.error = _this.posts = null;
        _this.loading = true;

        _this.$axios.get('/api/posts/categories/' + _this.slug, {
          params: {
            page: _this.currentPage,
            perPage: _this.perPage
          }
        }).then(function (response) {
          _this.loading = false;
          _this.posts = response.data.data;
          _this.currentPage = response.data.meta.current_page;
          _this.rows = response.data.meta.total;
        })["catch"](function (error) {
          _this.loading = false; // this.error = error.toString();

          console.log(error);
        });
      })["catch"](function (error) {
        _this.loading = false; // this.error = error.toString();

        console.log(error);
      });
    },
    itemsForList: function itemsForList() {
      if (this.posts) {
        return this.posts;
      }
    },
    updatePage: function updatePage(currentPage) {
      this.fetchData(currentPage);
    }
  }
});

/***/ }),

/***/ "./resources/js/Pages/PostsCategories.vue":
/*!************************************************!*\
  !*** ./resources/js/Pages/PostsCategories.vue ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _PostsCategories_vue_vue_type_template_id_234c8c36_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true& */ "./resources/js/Pages/PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true&");
/* harmony import */ var _PostsCategories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PostsCategories.vue?vue&type=script&lang=js& */ "./resources/js/Pages/PostsCategories.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _PostsCategories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _PostsCategories_vue_vue_type_template_id_234c8c36_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _PostsCategories_vue_vue_type_template_id_234c8c36_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "234c8c36",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/PostsCategories.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/PostsCategories.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/PostsCategories.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PostsCategories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PostsCategories.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PostsCategories.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PostsCategories_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/Pages/PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/Pages/PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PostsCategories_vue_vue_type_template_id_234c8c36_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PostsCategories_vue_vue_type_template_id_234c8c36_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PostsCategories_vue_vue_type_template_id_234c8c36_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PostsCategories.vue?vue&type=template&id=234c8c36&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "category container" },
    [
      _vm.category
        ? _c("h1", { staticClass: "text-center mb-5" }, [
            _vm._v("Category: " + _vm._s(_vm.category.name))
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.rows ? _c("h3", [_vm._v("Pages: " + _vm._s(_vm.rows))]) : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "post" }, [
        _vm.loading
          ? _c("div", { staticClass: "loading" }, [
              _vm._v("\n            Загрузка...\n        ")
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.error
          ? _c("div", { staticClass: "error" }, [
              _vm._v("\n            " + _vm._s(_vm.error) + "\n        ")
            ])
          : _vm._e()
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "d-flex flex-column" },
        _vm._l(_vm.itemsForList(), function(post) {
          return _c(
            "div",
            {},
            [
              _c(
                "b-card",
                {
                  staticClass: "mb-2",
                  attrs: {
                    id: "content",
                    "img-src": post.images[0].url,
                    "img-alt": "Image",
                    "img-left": "",
                    tag: "article",
                    "img-width": "500px"
                  }
                },
                [
                  _c("b-card-text", [_c("h3", [_vm._v(_vm._s(post.title))])]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {},
                    [
                      _c("b-card-text", [
                        _c("h6", [_vm._v("Categories")]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          { staticClass: "list-inline" },
                          _vm._l(post.categories, function(category) {
                            return _c(
                              "li",
                              { staticClass: "list-inline-item" },
                              [
                                _c(
                                  "router-link",
                                  {
                                    attrs: {
                                      to: {
                                        name: "post-categories",
                                        params: { slug: category.slug }
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(category.name))]
                                )
                              ],
                              1
                            )
                          }),
                          0
                        )
                      ]),
                      _vm._v(" "),
                      _c("b-card-text", {}, [
                        _c("h6", [_vm._v("Tags")]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          { staticClass: "list-inline" },
                          _vm._l(post.tags, function(tag) {
                            return _c(
                              "li",
                              { staticClass: "list-inline-item" },
                              [
                                _c("a", { attrs: { href: "" } }, [
                                  _vm._v(_vm._s(tag.name))
                                ])
                              ]
                            )
                          }),
                          0
                        )
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("b-card-text", [
                    _vm._v(
                      "\n                    " +
                        _vm._s(post.content.substring(0, 100) + "...") +
                        "\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "router-link",
                    {
                      attrs: { to: { name: "post", params: { id: post.id } } }
                    },
                    [
                      _c("b-button", { attrs: { variant: "primary" } }, [
                        _vm._v("Go somewhere")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        }),
        0
      ),
      _vm._v(" "),
      _c("b-pagination", {
        staticClass: "mt-5",
        attrs: {
          "total-rows": _vm.rows,
          "per-page": _vm.perPage,
          "aria-controls": "content",
          align: "center"
        },
        on: {
          input: function($event) {
            return _vm.updatePage(_vm.currentPage)
          }
        },
        model: {
          value: _vm.currentPage,
          callback: function($$v) {
            _vm.currentPage = $$v
          },
          expression: "currentPage"
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
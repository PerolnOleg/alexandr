const routes = [
    {
        path: '',
        component: () => import('../Pages/Home.vue'),
        name: 'home'
    },
    {
        path: 'about',
        component: () => import('../Pages/About.vue'),
        name: 'about'
    },
    {
        path: '/posts/categories/:slug',
        component: () => import('../Pages/PostsCategories.vue'),
        name: 'post-categories',
        props: true
    },
    {
        path: '/post/:id',
        component: () => import('../Pages/Post.vue'),
        name: 'post',
        props: true
    },
    {
        path: '/category',
        component: () => import('../Pages/Category.vue'),
        name: 'category'
    }

]

export default routes;

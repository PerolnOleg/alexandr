<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\PostCollection;
use App\Models\Category;
use App\Models\Post;


class CommonPageController extends Controller
{
    /**
     * @return PostCollection
     */
    public function homePage()
    {
        return new PostCollection(Post::with('user', 'categories', 'tags', 'images')
            ->paginate(request()->input('perPage', 15)));
    }

    /**
     * @param Category $category
     * @return PostCollection
     */
    public function categoryPage(Category $category)
    {
        return new PostCollection(Post::whereHas('categories', function ($query) use ($category) {
            $query->where('id', $category->id);
        })
            ->with('user', 'categories', 'tags', 'images')
            ->paginate(request()->input('perPage', 15)));
    }
}

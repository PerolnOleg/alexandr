<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\{CommonPageController, PostController, CategoryController, AuthController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});
Route::put('/{category}/category/update', [App\Http\Controllers\Api\CategoryController::class, 'update'])->middleware(['auth:api', 'can:update,category']);
Route::get('/home', [CommonPageController::class, 'homePage']);
Route::get('/posts/categories/{category:slug}', [CommonPageController::class, 'categoryPage']);

Route::resource('/posts', PostController::class);
Route::resource('/categories', CategoryController::class)->scoped(['category' => 'slug']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

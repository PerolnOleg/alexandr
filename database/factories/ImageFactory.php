<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Log;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        if (isset($caller['class'])) Log::info(" in {$caller['class']}");
        return [
            'title' => $this->faker->words(rand(1,2),true),
            'url' => $this->faker->imageUrl(640, 480, 'fake', true),
            'description' => $this->faker->text('100')
        ];
    }
}

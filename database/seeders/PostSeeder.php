<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Image;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags_id = Tag::where('active', 1)->pluck('id')->toArray();
        $categories_id = Category::where('active', 1)->pluck('id')->toArray();
        $users_id = User::pluck('id')->toArray();
        if ($tags_id && $categories_id && $users_id) {
            $posts = Post::factory()
                ->count(rand(50, 100))
                ->has(
                    Image::factory()->count(1),
                    'images'
                )
                ->state(new Sequence(function () use ($users_id) {
                    return [
                        'user_id' => Arr::random($users_id),
                    ];
                }))
                ->create();
            $posts->each(function ($post) use ($tags_id, $categories_id, $users_id) {
                $post->tags()->attach(Arr::random($tags_id, rand(1, 10)));
                $post->categories()->attach(Arr::random($categories_id, rand(1, 3)));
            });
        }

    }
}
